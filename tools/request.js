(function () {
    'use strict';
    function request(method) {
        let url = document.getElementById('request-url').value;
        let dataType = document.getElementById('request-data-type').value;
        let parameter = document.getElementById('request-parameter').value;
        if (url) {
            let api = 'request.php';
            let request_body =
                'url=' + encodeURIComponent(url)
                + '&method=' + encodeURIComponent(method)
                + '&type=' + encodeURIComponent(dataType)
                + '&parameter=' + encodeURIComponent(parameter);
            let headers = {
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
            };
            fetch(api, {
                method: 'POST',
                headers: headers,
                body: request_body,
                credentials: 'include'
            }).then(function (response) {
                return response.text();
            }, function (err) {
                console.error(err);
            }).then(function (text) {
                document.getElementById('request-result').innerText = text;
            });
        } else {
            console.log(url);
            console.log(dataType);
            console.log(parameter);
        }
    }

    let buttons = document.getElementsByTagName('button');
    for (let i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', (e) => request(e.target.getAttribute('id').replace(/^request-method-/, '')), false);
    }
})();