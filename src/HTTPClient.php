<?php
/**
 * Created by PhpStorm.
 * User: m.kiba
 * Date: 2018/10/18
 * Time: 13:08
 */

namespace Lumberyard;

/**
 * Class HTTPClient
 * @package Lumberyard
 */
class HTTPClient
{
    const PROXY_URL = 10;
    const COOKIE_FILE = 20;
    const USER_AGENT = 30;
    const AUTO_REDIRECT = 40;
    const MAX_REDIRECT = 41;
    const NO_REFERER = 42;
    const ACCEPT_MIME = 50;
    const ACCEPT_LANGUAGE = 51;
    const ACCEPT_CHARSET = 52;
    const ENABLE_PROGRESS = 60;

    private $load_buffer_size = 4096;
    private $options = [];
    private $history = [];
    private $timeout = 20;
    private $requests = [];
    private $CRLF = "\r\n";

    public function __construct($options = [])
    {
        $this->options = $options;
        if (!isset($this->options[$this::PROXY_URL]))
            $this->options[$this::PROXY_URL] = null;
        if (!isset($this->options[$this::COOKIE_FILE]))
            $this->options[$this::COOKIE_FILE] = null;
        if (!isset($this->options[$this::USER_AGENT]))
            $this->options[$this::USER_AGENT] = 'Mozilla/5.0 (' . php_uname() . ') PHP-HTTPClient/1.0';
        if (!isset($this->options[$this::NO_REFERER]))
            $this->options[$this::NO_REFERER] = false;
        if (!isset($this->options[$this::AUTO_REDIRECT]))
            $this->options[$this::AUTO_REDIRECT] = false;
        if (!isset($this->options[$this::MAX_REDIRECT]))
            $this->options[$this::MAX_REDIRECT] = 10;
        if (!isset($this->options[$this::ACCEPT_MIME]))
            $this->options[$this::ACCEPT_MIME] = ['text/html', 'application/xhtml+xml', 'application/xml;q=0.9', '*/*;q=0.8'];
        if (!isset($this->options[$this::ACCEPT_LANGUAGE]))
            $this->options[$this::ACCEPT_LANGUAGE] = ['ja', 'en-US;q=0.7', 'en;q=0.3', '*;q=0.1'];
        if (!isset($this->options[$this::ACCEPT_CHARSET]))
            $this->options[$this::ACCEPT_CHARSET] = ['utf-8', 'shift_jis;q=0.7', 'euc-jp;q=0.7', 'iso-2022-jp;q=0.7', '*;q=0.2'];
        if (!isset($this->options[$this::ENABLE_PROGRESS]))
            $this->options[$this::ENABLE_PROGRESS] = false;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function setOption($key, $value)
    {
        $this->options[$key] = $value;
    }

    /**
     * @param string|null $url
     * @param string|null $method
     * @return \Lumberyard\HTTPClient\Request
     */
    public function request($url = null, $method = null)
    {
        return new HTTPClient\Request($method, $url, $this);
    }

    /**
     * @return array
     */
    public function lastRequestHeader()
    {
        return $this->requests;
    }

    /**
     * @param \Lumberyard\HTTPClient\Request $request
     * @param int $count_redirect
     * @param \Lumberyard\HTTPClient\Response $redirect_response
     * @return \Lumberyard\HTTPClient\Response
     */
    public function send($request, $count_redirect = 0, $redirect_response = null)
    {
        if ($count_redirect === 0) {
            $this->requests = [];
        }
        $cookies = null;
        if ($this->options[$this::COOKIE_FILE]) {
            $cookies = new HTTPClient\Cookies($this->options[$this::COOKIE_FILE]);
            $send_cookies = $cookies->getCookies($request->url);
            $parts = [];
            foreach ($send_cookies as $key => $value) {
                $parts[] = $key . '=' . $value;
            }
            if ($parts)
                $request->headers->set('Cookie', join('; ', $parts));
        }
        $request->headers->set('Host', $request->host, true);
        if ($this->options[$this::USER_AGENT])
            $request->headers->set('User-Agent', $this->options[$this::USER_AGENT], false);
        if ($this->options[$this::ACCEPT_MIME])
            $request->headers->set('Accept', $this->options[$this::ACCEPT_MIME], false);
        if ($this->options[$this::ACCEPT_LANGUAGE])
            $request->headers->set('Accept-Language', $this->options[$this::ACCEPT_LANGUAGE], false);
        if ($this->options[$this::ACCEPT_CHARSET])
            $request->headers->set('Accept-Charset', $this->options[$this::ACCEPT_CHARSET], false);
        $request->headers->set('Connection', 'keep-alive', false);
        if (!$this->options[$this::NO_REFERER] and $this->history) {
            $referer = $this->history[count($this->history) - 1];
            if ($referer and (preg_match('/^https/', $request->url) or !preg_match('/^https/', $referer))) {
                $request->headers->set('Referer', $referer, false);
            }
        }
        $response = new HTTPClient\Response($request->url);
        if ($redirect_response)
            $response->setRedirected($redirect_response);
        $sock = null;
        if (isset($this->options[$this::PROXY_URL])) {
            if ($request->isSecure) {
                $sock = $this->_connect_ssl_via_proxy($request, $response);
            } else {
                $sock = $this->_connect_via_proxy($request, $response);
            }
        } else {
            if ($request->isSecure) {
                $sock = $this->_connect_ssl($request, $response);
            } else {
                $sock = $this->_connect($request, $response);
            }
        }
        if ($sock) {
            $body = null;
            if ($request->hasBody) {
                $body = $this->_request_body($request);
            }
            $this->_send_header($sock, $request->headers->list());
            if ($body) {
                $this->_send_body($sock, $body);
                fclose($body);
            }
            $this->_receive($response, $sock);
            fclose($sock);
        }
        $response->fix();
        if ($cookies) {
            $set_cookies = $response->headers->get('Set-Cookie');
            if ($set_cookies) {
                for ($i = 0; $i < $set_cookies->length; $i++) {
                    $set_cookie = $set_cookies[$i];
                    $parts = explode(';', $set_cookie);
                    $cookie = [];
                    $kv = explode('=', trim(array_shift($parts)), 2);
                    $cookie['key'] = $kv[0];
                    $cookie['value'] = (!isset($kv[1]) or $kv[1] === '') ? null : $kv[1];
                    foreach ($parts as $part) {
                        $kv = explode('=', trim($part), 2);
                        $key = strtolower($kv[0]);
                        switch ($key) {
                            case 'domain':
                            case 'path':
                            case 'expires':
                            case 'max-age':
                                if (isset($kv[1]))
                                    $cookie[$key] = $kv[1];
                                break;
                            case 'httponly':
                            case 'secure':
                                $cookie[$key] = true;
                                break;
                            default:
                                $cookie[$kv[0]] = (!isset($kv[1]) or $kv[1] === '') ? null : $kv[1];
                                break;
                        }
                    }
                    if (isset($cookie['key']))
                        $cookies->add($request->URL, $cookie);
                }
            }
        }
        $this->history[] = $request->url;
        if ($this->options[$this::AUTO_REDIRECT] and $count_redirect < $this->options[$this::MAX_REDIRECT]) {
            $count_redirect++;
            switch ($response->status) {
                case 301:
                case 302:
                case 303:
                    $redirect_location = $this->_to_absolute_url($request, $response->headers->get('Location'));
                    $new_request = $this->request($redirect_location);
                    return $this->send($new_request, $count_redirect, $response);
                    break;
                case 307:
                case 308:
                    $redirect_location = $this->_to_absolute_url($request, $response->headers->get('Location'));
                    $new_request = clone $request;
                    $new_request->URL = $redirect_location;
                    $new_request->headers->remove('Cookie');
                    return $this->send($new_request, $count_redirect, $response);
                    break;
                default:
                    return $response;
                    break;
            }
        } else {
            return $response;
        }
    }

    /**
     * @param \Lumberyard\HTTPClient\Request $request
     * @param string $path
     * @return string
     */
    private function _to_absolute_url($request, $path)
    {
        if (preg_match('#^https?://#', $path)) {
            return $path;
        }
        if (preg_match('#^//#', $path)) {
            return $request->scheme . '://' . $path;
        }
        if (preg_match('#^/#', $path)) {
            $url = $request->scheme . '://' . $request->host;
            if (($request->scheme === 'https' and $request->port !== 443)
                or ($request->scheme === 'http' and $request->port !== 80)
                or ($request->scheme !== 'http' and $request->scheme !== 'https')) {
                $url .= ':' . $request->port;
            }
            $url .= $path;
            return $url;
        }
        $url = $request->scheme . '://' . $request->host;
        if (($request->scheme === 'https' and $request->port !== 443)
            or ($request->scheme === 'http' and $request->port !== 80)
            or ($request->scheme !== 'http' and $request->scheme !== 'https')) {
            $url .= ':' . $request->port;
        }
        $dir = [];
        foreach (explode('/', dirname($request->path) . '/' . $path) as $d) {
            if ($d === '.')
                continue;
            elseif ($d === '..' and count($dir))
                array_pop($dir);
            elseif ($d === '..')
                continue;
            else
                $dir[] = $d;
        }
        $path = preg_replace('#//+#', '/', join('/', $dir));
        if ($path === '')
            $path = '/';
        $url .= $path;
        return $url;
    }

    /**
     * @param resource $sock
     * @param string $request_string
     */
    private function _request($sock, $request_string = '')
    {
        //echo $request_string, $this->CRLF;
        $this->requests[] = $request_string;
        fwrite($sock, $request_string . $this->CRLF);
    }

    /**
     * @param \Lumberyard\HTTPClient\Request $request
     * @param \Lumberyard\HTTPClient\Response $response
     * @return resource
     */
    private function _connect_ssl_via_proxy($request, $response)
    {
        $parsed = parse_url($this->options[$this::PROXY_URL]);
        $proxy_host = $parsed['host'];
        $proxy_port = isset($parsed['port']) ? intval($parsed['port']) : 0;
        if ($proxy_port === 0)
            $proxy_port = 8080;
        $remote_socket = 'tcp://' . $proxy_host . ':' . $proxy_port;
        $context = stream_context_create([
            'ssl' => [
                'verify_host' => false,
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ]);
        $sock = stream_socket_client($remote_socket, $err_no, $err_str, $this->timeout, STREAM_CLIENT_CONNECT, $context);
        if (!$sock) {
            $response->setError('Proxy connection error:', $this->options[$this::PROXY_URL]);
            return null;
        }
        $this->_request($sock, sprintf('%s %s:%s HTTP/1.1', 'CONNECT', $request->host, $request->port));
        $this->_request($sock, sprintf('Host: %s', $proxy_host));
        $this->_request($sock, sprintf('User-Agent: %s', $this->options[$this::USER_AGENT]));
        $this->_request($sock, 'Proxy-Connection: keep-alive');
        if (isset($parsed['user']) or isset($parsed['pass'])) {
            $user = isset($parsed['user']) ? $parsed['user'] : '';
            $pass = isset($parsed['pass']) ? $parsed['pass'] : '';
            $this->_request($sock, sprintf('Proxy-Authorization: Basic %s', base64_encode($user . ':' . $pass)));
        }
        $this->_request($sock);
        while (!feof($sock)) {
            $buffer = rtrim(fgets($sock));
            if ($buffer === '')
                break;
            $response->addHeader($buffer);
        }
        if ($response->status < 200 or $response->status >= 300) {
            $response->setError('Proxy status error:', $response->status);
            return null;
        }
        $modes = [
            STREAM_CRYPTO_METHOD_TLS_CLIENT,
            STREAM_CRYPTO_METHOD_SSLv3_CLIENT,
            STREAM_CRYPTO_METHOD_SSLv23_CLIENT,
            STREAM_CRYPTO_METHOD_SSLv2_CLIENT
        ];
        foreach ($modes as $mode) {
            if (stream_socket_enable_crypto($sock, true, $mode) === true)
                break;
            stream_socket_enable_crypto($sock, false, $mode);
        }
        $this->_request($sock, sprintf('%s %s HTTP/1.1', $request->method, $request->uri));
        return $sock;
    }

    /**
     * @param \Lumberyard\HTTPClient\Request $request
     * @param \Lumberyard\HTTPClient\Response $response
     * @return resource
     */
    private function _connect_via_proxy($request, $response)
    {
        $parsed = parse_url($this->options[$this::PROXY_URL]);
        $proxy_host = $parsed['host'];
        $proxy_port = isset($parsed['port']) ? intval($parsed['port']) : 0;
        if ($proxy_port === 0)
            $proxy_port = 8080;
        $remote_socket = 'tcp://' . $proxy_host . ':' . $proxy_port;
        $sock = stream_socket_client($remote_socket, $err_no, $err_str, $this->timeout, STREAM_CLIENT_CONNECT);
        if (!$sock) {
            $response->setError('Proxy connection error:', $this->options[$this::PROXY_URL]);
            return null;
        }
        $this->_request($sock, sprintf('%s %s HTTP/1.1', $request->method, preg_replace('/#.*$/', '', $request->url)));
        if (isset($parsed['user']) or isset($parsed['pass'])) {
            $user = isset($parsed['user']) ? $parsed['user'] : '';
            $pass = isset($parsed['pass']) ? $parsed['pass'] : '';
            $this->_request($sock, sprintf('Proxy-Authorization: Basic %s', base64_encode($user . ':' . $pass)));
        }
        $this->_request($sock, 'Proxy-Connection: keep-alive');
        return $sock;
    }

    /**
     * @param \Lumberyard\HTTPClient\Request $request
     * @param \Lumberyard\HTTPClient\Response $response
     * @return resource
     */
    private function _connect_ssl($request, $response)
    {
        $err_no = 0;
        $err_str = null;
        $remote_socket = 'ssl://' . $request->host . ':' . $request->port;
        $context = stream_context_create();
        stream_context_set_option($context, 'ssl', 'verify_host', false);
        stream_context_set_option($context, 'ssl', 'verify_peer', false);
        stream_context_set_option($context, 'ssl', 'allow_self_signed', true);
        $sock = stream_socket_client($remote_socket, $err_no, $err_str, $this->timeout, STREAM_CLIENT_CONNECT, $context);
        if (!$sock) {
            $response->setError('HTTPS connection error');
            return null;
        }
        $this->_request($sock, sprintf('%s %s HTTP/1.1', $request->method, $request->uri));
        return $sock;
    }

    /**
     * @param \Lumberyard\HTTPClient\Request $request
     * @param \Lumberyard\HTTPClient\Response $response
     * @return resource
     */
    private function _connect($request, $response)
    {
        $err_no = 0;
        $err_str = null;
        $remote_socket = 'tcp://' . $request->host . ':' . $request->port;
        $sock = stream_socket_client($remote_socket, $err_no, $err_str, $this->timeout, STREAM_CLIENT_CONNECT);
        if (!$sock) {
            $response->setError('HTTP connection error');
            return null;
        }
        $this->_request($sock, sprintf('%s %s HTTP/1.1', $request->method, $request->uri));
        return $sock;
    }

    /**
     * @param \Lumberyard\HTTPClient\Request $request
     * @return resource
     */
    private function _request_body($request)
    {
        if (!is_null($request->file)) {
            return $this->_request_body_by_file($request);
        } elseif ($request->body) {
            return $this->_request_body_by_string($request);
        } elseif ($request->headers->get('Content-Type') === 'application/json') {
            return $this->_request_body_json($request);
        } elseif ($request->isAttached) {
            return $this->_request_body_multipart($request);
        } else {
            return $this->_request_body_query($request);
        }
    }

    /**
     * @param \Lumberyard\HTTPClient\Request $request
     * @return resource
     */
    private function _request_body_by_file($request)
    {
        $size = 0;
        $body = fopen('php://temp', 'wb');
        $fp = fopen($request->file, 'r');
        if (!$fp)
            return null;
        flock($fp, LOCK_SH);
        while (!feof($fp)) {
            $buffer = fread($fp, 4096);
            $size += fwrite($body, $buffer);
        }
        fclose($fp);
        $request->headers->set('Content-Type', mime_content_type($request->file), true);
        $request->headers->set('Content-Length', $size, true);
        return $body;
    }

    /**
     * @param \Lumberyard\HTTPClient\Request $request
     * @return resource
     */
    private function _request_body_by_string($request)
    {
        $size = 0;
        $body = fopen('php://temp', 'wb');
        $size += fwrite($body, $request->body);
        $request->headers->set('Content-Type', 'application/octet-stream', false);
        $request->headers->set('Content-Length', $size, true);
        return $body;
    }


    /**
     * @param \Lumberyard\HTTPClient\Request $request
     * @return resource
     */
    private function _request_body_json($request)
    {
        $size = 0;
        $body = fopen('php://temp', 'wb');
        $size += fwrite($body, json_encode(array_map([$this, 'file2base64'], $request->parameters), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
        $request->headers->set('Content-Type', 'application/json', true);
        $request->headers->set('Content-Length', $size);
        return $body;
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    private function file2base64($value)
    {
        if (is_array($value)) {
            return array_map([$this, 'file2base64'], $value);
        } elseif ($value instanceof \CURLFile) {
            return base64_encode(file_get_contents($value->name));
        } else {
            return $value;
        }
    }

    /**
     * @param \Lumberyard\HTTPClient\Request $request
     * @return resource
     */
    private function _request_body_multipart($request)
    {
        $boundary = str_repeat('-', rand(4, 16));
        $letters = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        for ($i = 0; $i < rand(24, 32); $i++) {
            $boundary .= $letters[array_rand($letters)];
        }
        $body = fopen('php://temp', 'wb');
        $size = $this->_multipart($body, $request->parameters, $boundary);
        $request->headers->set('Content-Type', 'multipart/form-data; boundary=' . $boundary, true);
        $request->headers->set('Content-Length', $size);
        return $body;
    }

    /**
     * @param resource $body
     * @param array $parameters
     * @param string $boundary
     * @param string $parent_key
     * @return int
     */
    private function _multipart($body, $parameters, $boundary, $parent_key = '')
    {
        $size = 0;
        foreach (array_keys($parameters) as $key) {
            $current_key = $key;
            if ($parent_key) {
                $current_key = $parent_key . '[' . $key . ']';
            }
            $value = $parameters[$key];
            if (is_array($value)) {
                $size += $this->_multipart($body, $value, $boundary, $current_key);
            } elseif ($value instanceof \CURLFile) {
                $size += fwrite($body, '--' . $boundary . $this->CRLF);
                $size += fwrite($body, 'Content-Disposition: form-data; name="' . $this->_escape_multipart_header($current_key) . '"; filename="' . $this->_escape_multipart_header(basename($value->name)) . '"' . $this->CRLF);
                $size += fwrite($body, 'Content-Type: ' . $value->mime . $this->CRLF);
                $size += fwrite($body, 'Content-Transfer-Encoding: binary' . $this->CRLF);
                $size += fwrite($body, $this->CRLF);
                $fp = fopen($value->name, 'r');
                flock($fp, LOCK_SH);
                while (!feof($fp)) {
                    $size += fwrite($body, fread($fp, 4096));
                }
                $size += fwrite($body, $this->CRLF);
                fclose($fp);
            } else {
                $size += fwrite($body, '--' . $boundary . $this->CRLF);
                $size += fwrite($body, 'Content-Disposition: form-data; name="' . $this->_escape_multipart_header($current_key) . '"' . $this->CRLF);
                $size += fwrite($body, $this->CRLF);
                $size += fwrite($body, $value . $this->CRLF);
            }
        }
        if ($parent_key === '') {
            $size += fwrite($body, '--' . $boundary . '--' . $this->CRLF);
        }
        return $size;
    }

    /**
     * @param string $string
     * @return string
     */
    private function _escape_multipart_header($string)
    {
        return preg_replace(['/"/', '/(\n|\r)/'], ['\\"', ''], $string);
    }

    /**
     * @param \Lumberyard\HTTPClient\Request $request
     * @return resource
     */
    private function _request_body_query($request)
    {
        $size = 0;
        $body = fopen('php://temp', 'wb');
        $size += fwrite($body, http_build_query($request->parameters));
        $request->headers->set('Content-Type', 'application/x-www-form-urlencoded', true);
        $request->headers->set('Content-Length', $size);
        return $body;
    }

    /**
     * @param array $request_headers
     * @param resource $sock
     */
    private function _send_header($sock, $request_headers)
    {
        foreach ($request_headers as $header) {
            $this->_request($sock, $header);
        }
        $this->_request($sock);
    }

    /**
     * @param resource $sock
     * @param resource $body
     */
    private function _send_body($sock, $body)
    {
        fseek($body, 0, SEEK_SET);
        while (!feof($body)) {
            fwrite($sock, fread($body, 4096));
        }
    }

    /**
     * @param \Lumberyard\HTTPClient\Response $response
     * @param resource $sock
     */
    private function _receive($response, $sock)
    {
        $start = microtime(true);
        $chunked = false;
        $contentLength = -1;
        while (!feof($sock)) {
            $line = rtrim(fgets($sock));
            if ($line === '') {
                break;
            }
            $response->addHeader($line);
            if (preg_match('/^Transfer\-Encoding:(.+)/i', $line, $m)) {
                $encodings = array_map(function ($v) {
                    return trim($v);
                }, explode(',', $m[1]));
                if (in_array('chunked', $encodings)) {
                    $chunked = true;
                }
            }
            if (preg_match('/^Content\-Length:(.+)/i', $line, $m)) {
                if (preg_match('/^[0-9]+$/i', trim($m[1]))) {
                    $contentLength = intval($m[1]);
                }
            }
        }
        if ($chunked) {
            $chunks = [];
            while (true) {
                $chunk16 = rtrim(fgets($sock));
                if (!preg_match('/^[0-9a-f]+$/', $chunk16)) {
                    continue;
                }
                $chunk10 = hexdec($chunk16);
                $chunks[] = [$chunk16, $chunk10];
                if ($chunk10 === 0)
                    break;
                while ($chunk10 > 0) {
                    if ($chunk10 > $this->load_buffer_size)
                        $chunk10 -= $response->addContent(fread($sock, $this->load_buffer_size));
                    else
                        $chunk10 -= $response->addContent(fread($sock, $chunk10));
                    if (feof($sock))
                        break;
                }
                fgets($sock);
            }
        } elseif ($contentLength >= 0) {
            while ($contentLength > 0) {
                if ($contentLength > $this->load_buffer_size)
                    $contentLength -= $response->addContent(fread($sock, $this->load_buffer_size));
                else
                    $contentLength -= $response->addContent(fread($sock, $contentLength));
                if (feof($sock))
                    break;
            }
        } else {
            while (!feof($sock)) {
                $response->addContent(fread($sock, $this->load_buffer_size));
            }
        }
        $end = microtime(true);
        $response->setResponseTime($end - $start);
    }
}
