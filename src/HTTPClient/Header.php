<?php
/**
 * Created by PhpStorm.
 * User: m.kiba
 * Date: 2018/10/18
 * Time: 13:49
 */

namespace Lumberyard\HTTPClient;

/**
 * Class Header
 * @package Lumberyard\HTTPClient
 * @property-read int $length
 */
class Header implements \ArrayAccess, \IteratorAggregate
{
    private $container = [];

    /**
     * Header constructor.
     * @param array $arr
     */
    public function __construct($arr = [])
    {
        $this->container = $arr;
    }

    /**
     * @param string $property
     * @return mixed
     */
    public function __get($property)
    {
        if ($property === 'length')
            return count($this->container);
        return null;
    }

    /**
     * @param mixed $index
     * @return mixed
     */
    public function offsetGet($index)
    {
        return isset($this->container[$index]) ? $this->container[$index] : null;
    }

    /**
     * @param mixed $index
     * @return bool
     */
    public function offsetExists($index)
    {
        return isset($this->container[$index]);
    }

    /**
     * @param mixed $index
     * @param mixed $value
     */
    public function offsetSet($index, $value)
    {
        if (is_null($index)) {
            $this->container[] = $value;
        } else {
            $this->container[$index] = $value;
        }
    }

    /**
     * @param mixed $index
     */
    public function offsetUnset($index)
    {
        unset($this->container[$index]);
    }

    /**
     * @return mixed
     */
    public function pop()
    {
        return array_pop($this->container);
    }

    /**
     * @param mixed ...$values
     * @return int
     */
    public function push(...$values)
    {
        return array_push($this->container, ...$values);
    }

    /**
     * @return mixed
     */
    public function shift()
    {
        return array_shift($this->container);
    }

    /**
     * @param mixed ...$values
     * @return int
     */
    public function unshift(...$values)
    {
        return array_unshift($this->container, ...$values);
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->container);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        // TODO: Implement __toString() method.
        return join(',', array_values($this->container));
    }
}