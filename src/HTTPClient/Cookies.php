<?php
/**
 * Created by PhpStorm.
 * User: m.kiba
 * Date: 2018/10/18
 * Time: 17:23
 */

namespace Lumberyard\HTTPClient;

/**
 * Class Cookies
 * @package Lumberyard\HTTPClient
 */
class Cookies
{
    private $cookies = [];
    private $cookie_file = null;

    /**
     * Cookies constructor.
     * @param string $cookie_file
     */
    public function __construct($cookie_file)
    {
        $this->cookie_file = $cookie_file;
        if (is_readable($cookie_file)) {
            $this->cookies = $this->load();
        }
    }

    /**
     *
     */
    public function __destruct()
    {
        // TODO: Implement __destruct() method.
        if ((file_exists($this->cookie_file) and is_writable($this->cookie_file)) or is_writable(dirname($this->cookie_file))) {
            $this->save();
        }
    }

    /**
     * @return array
     */
    private function load()
    {
        $fp = fopen($this->cookie_file, 'rb');
        flock($fp, LOCK_SH);
        $buffer = fread($fp, filesize($this->cookie_file));
        $cookies = @json_decode($buffer, true);
        if (!$cookies)
            $cookies = [];
        foreach (array_keys($cookies) as $domain) {
            foreach (array_keys($cookies[$domain]) as $path) {
                foreach (array_keys($cookies[$domain][$path]) as $name) {
                    if ($cookies[$domain][$path][$name]['expires'] < time()) {
                        unset($cookies[$domain][$path][$name]);
                    }
                }
            }
        }
        fclose($fp);
        return $cookies;
    }

    /**
     * @return bool
     */
    private function save()
    {
        $fp = fopen($this->cookie_file, 'cb+');
        flock($fp, LOCK_EX);
        $buffer = '';
        while (!feof($fp)) {
            $buffer .= fread($fp, 4096);
        }
        $cookies = @json_decode($buffer, true);
        if (!$cookies)
            $cookies = [];
        foreach (array_keys($cookies) as $domain) {
            foreach (array_keys($cookies[$domain]) as $path) {
                foreach (array_keys($cookies[$domain][$path]) as $name) {
                    if ($cookies[$domain][$path][$name]['expires'] < time()) {
                        unset($cookies[$domain][$path][$name]);
                    }
                }
            }
        }
        foreach (array_keys($this->cookies) as $domain) {
            if (!isset($cookies[$domain]))
                $cookies[$domain] = [];
            foreach (array_keys($this->cookies[$domain]) as $path) {
                if (!isset($cookies[$domain][$path]))
                    $cookies[$domain][$path] = [];
                foreach (array_keys($this->cookies[$domain][$path]) as $name) {
                    if ($this->cookies[$domain][$path][$name]['expires'] >= time()) {
                        $cookies[$domain][$path][$name] = $this->cookies[$domain][$path][$name];
                    }
                }
            }
        }
        fseek($fp, 0, SEEK_SET);
        ftruncate($fp, 0);
        rewind($fp);
        fwrite($fp, json_encode($cookies, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        fclose($fp);
        return true;
    }

    /**
     * @param string $url
     * @param array $cookie
     */
    public function add($url, $cookie)
    {
        $key = isset($cookie['key']) ? $cookie['key'] : null;
        $value = isset($cookie['value']) ? $cookie['value'] : null;
        if (is_null($key)) {
            return;
        }
        $parsed = parse_url($url);
        $domain = isset($cookie['domain']) ? $cookie['domain'] : $parsed['host'];
        $domain = preg_replace('/^\.+/', '', $domain);
        $path = isset($cookie['path']) ? $cookie['path'] : dirname($parsed['path']);
        $host_only = !(isset($cookie['domain']));
        $http_only = (isset($cookie['httponly']) and $cookie['httponly'] === true);
        $ssl_only = (isset($cookie['secure']) and $cookie['secure'] === true);
        $expires = isset($cookie['expires']) ? strtotime($cookie['expires']) : null;
        if (is_null($expires) or $expires === false) {
            $expires = isset($cookie['max-age']) ? time() + intval($cookie['max-age']) : null;
        }
        if (!isset($this->cookies[$domain])) {
            $this->cookies[$domain] = [];
        }
        if (!isset($this->cookies[$domain][$path])) {
            $this->cookies[$domain][$path] = [];
        }
        if (!isset($this->cookies[$domain][$path][$key])) {
            $this->cookies[$domain][$path][$key] = [];
        }
        if (is_null($value)) {
            unset($this->cookies[$domain][$path][$key]);
        } else {
            $this->cookies[$domain][$path][$key]['value'] = $value;
            $this->cookies[$domain][$path][$key]['expires'] = is_null($expires) ? -1 : $expires;
            $this->cookies[$domain][$path][$key]['ssl_only'] = $ssl_only;
            $this->cookies[$domain][$path][$key]['host_only'] = $host_only;
            $this->cookies[$domain][$path][$key]['http_only'] = $http_only;
        }
    }

    /**
     * @param string $url
     * @return array
     */
    public function getCookies($url)
    {
        $parsed = parse_url($url);
        $parsed_scheme = $parsed['scheme'];
        $parsed_host = $parsed['host'];
        $parsed_path = $parsed['path'];
        //
        $cookies = [];
        //
        $domains = array_keys($this->cookies);
        usort($domains, function ($a, $b) {
            $len_a = strlen($a);
            $len_b = strlen($b);
            if ($len_a === $len_b)
                return strcmp($a, $b);
            return $len_a < $len_b ? -1 : 1;
        });
        foreach ($domains as $domain) {
            if ($parsed_host !== $domain and !preg_match('/\.' . preg_quote($domain, '/') . '$/', $parsed_host))
                continue;
            $paths = array_keys($this->cookies[$domain]);
            usort($paths, function ($a, $b) {
                return strcmp($a, $b);
            });
            foreach ($paths as $path) {
                if ($path !== $parsed_path and preg_match('/^' . preg_quote($path, '/') . '\//', $parsed_path))
                    continue;
                foreach (array_keys($this->cookies[$domain][$path]) as $name) {
                    $cookie = $this->cookies[$domain][$path][$name];
                    if ($cookie['expires'] < time() and $cookie['expires'] !== -1)
                        continue;
                    if ($cookie['host_only'] === true and $parsed_host !== $domain)
                        continue;
                    if ($cookie['ssl_only'] === true and $parsed_scheme !== 'https')
                        continue;
                    $cookies[$name] = $cookie['value'];
                }
            }
        }
        //
        return $cookies;
    }
}