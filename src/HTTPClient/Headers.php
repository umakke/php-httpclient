<?php
/**
 * Created by PhpStorm.
 * User: m.kiba
 * Date: 2018/10/18
 * Time: 13:11
 */

namespace Lumberyard\HTTPClient;

/**
 * Class Header
 * @package Lumberyard\HTTPClient
 * @method bool add(string $header_key, string | array $header_value)
 * @method bool set(string $header_key, string | array $header_value, bool $is_overwrite = false)
 * @method bool remove(string $header_key)
 */
class Headers
{
    private $is_fixed = false;
    private $headers = [];

    public function __construct()
    {
    }

    /**
     * @param string $property_name
     * @return mixed
     */
    public function __get($property_name)
    {
        return $this->get($property_name);
    }

    /**
     * @param string $method_name
     * @param array $arguments
     * @return mixed
     */
    public function __call($method_name, $arguments = [])
    {
        if (!$this->is_fixed) {
            if ($method_name === 'add')
                return $this->_add(...$arguments);
            if ($method_name === 'set')
                return $this->_set(...$arguments);
            if ($method_name === 'remove')
                return $this->_remove(...$arguments);
        }
        return null;
    }

    /**
     * @return bool
     */
    public function fix()
    {
        return $this->is_fixed = true;
    }

    /**
     * @param string $header_key
     * @param string|array $header_value
     * @return bool
     */
    private function _add($header_key, $header_value)
    {
        $key = $this->normalize_key($header_key);
        if (!isset($this->headers[$key]))
            $this->headers[$key] = new Header();
        if (is_array($header_value)) {
            foreach ($header_value as $v) {
                $this->headers[$key]->push(trim($v));
            }
        } else {
            $this->headers[$key]->push(trim($header_value));
        }
        return true;
    }

    /**
     * @param string $header_key
     * @param string|array $header_value
     * @param bool $is_overwrite
     * @return bool
     */
    private function _set($header_key, $header_value, $is_overwrite = false)
    {
        $key = $this->normalize_key($header_key);
        if (isset($this->headers[$key]) and !$is_overwrite)
            return false;
        $this->headers[$key] = new Header();
        if (is_array($header_value)) {
            foreach ($header_value as $v) {
                $this->headers[$key]->push($v);
            }
        } else {
            $this->headers[$key]->push($header_value);
        }
        return true;
    }

    /**
     * @param string $header_key
     * @return bool
     */
    private function _remove($header_key)
    {
        $key = $this->normalize_key($header_key);
        if (isset($this->headers[$key]))
            unset($this->headers[$key]);
        return true;
    }

    /**
     * @param string $header_key
     * @return mixed
     */
    public function get($header_key = null)
    {
        if ($header_key === null) {
            return $this->headers;
        } else {
            $key = $this->normalize_key($header_key);
            return isset($this->headers[$key]) ? $this->headers[$key] : null;
        }
    }

    /**
     * @param string $header_key
     * @return string
     */
    private function normalize_key($header_key)
    {
        return join('-', array_map(function ($v) {
            return ucfirst($v);
        }, preg_split('/[^a-z0-9]+/', strtolower($header_key))));
    }

    /**
     * @return array
     */
    public function list()
    {
        $keys = array_keys($this->headers);
        usort($keys, function ($a, $b) {
            if ($a === 'Host') return -1;
            if ($b === 'Host') return 1;
            return strcmp($a, $b);
        });
        $headers = [];
        foreach ($keys as $key) {
            $headers[] = sprintf('%s: %s', $key, strval($this->headers[$key]));
        }
        return $headers;
    }

    /**
     * @return array
     */
    public function __debugInfo()
    {
        // TODO: Implement __debugInfo() method.
        return $this->list();
    }
}