<?php
/**
 * Created by PhpStorm.
 * User: m.kiba
 * Date: 2018/10/18
 * Time: 13:08
 */

namespace Lumberyard\HTTPClient;

/**
 * Class Request
 * @package Lumberyard\HTTPClient
 * @property string $method
 * @property string $url
 * @property string $body
 * @property string $file
 * @property string $METHOD
 * @property string $URL
 * @property string $BODY
 * @property string $FILE
 * @property-read string $scheme
 * @property-read string $host
 * @property-read string $port
 * @property-read string $path
 * @property-read string|null $query
 * @property-read string|null $fragment
 * @property-read string|null $user
 * @property-read string|null $password
 * @property-read string $uri
 * @property-read bool $isSecure
 * @property-read \Lumberyard\HTTPClient\Headers $headers
 * @property-read \Lumberyard\HTTPClient\Headers $header
 * @property-read array $parameters
 * @property-read bool $isAttached
 * @property-read bool $hasBody
 */
class Request
{
    private $headers = null;
    private $method = 'GET';
    private $url = null;
    private $client = null;
    private $scheme = null;
    private $host = null;
    private $port = null;
    private $path = null;
    private $query = null;
    private $fragment = null;
    private $user = null;
    private $password = null;
    private $parameters = [];
    private $body = null;
    private $file = null;
    private $isAttached = false;
    private $hasBody = false;

    /**
     * Request constructor.
     * @param string $method
     * @param string $url
     * @param \Lumberyard\HTTPClient $client
     */
    public function __construct($method = null, $url = null, $client = null)
    {
        if ($method)
            $this->set_method($method);
        if ($url)
            $this->set_url($url);
        if ($client)
            $this->client = $client;
        $this->headers = new Headers();
    }

    /**
     * @param string $property_name
     * @return mixed
     */
    public function __get($property_name)
    {
        if (property_exists($this, $property_name))
            return $this->$property_name;
        $property_name = strtolower($property_name);
        if (property_exists($this, $property_name))
            return $this->$property_name;
        if ($property_name === 'issecure')
            return $this->scheme === 'https' ? true : false;
        if ($property_name === 'uri')
            return is_null($this->query) ? $this->path : ($this->path . '?' . $this->query);
        return null;
    }

    /**
     * @param string $property_name
     * @param mixed $value
     * @return bool
     * @throws \Exception
     */
    public function __set($property_name, $value)
    {
        $property_name = strtolower($property_name);
        if ($property_name === 'method')
            return $this->set_method($value);
        if ($property_name === 'url')
            return $this->set_url($value);
        if ($property_name === 'body')
            return $this->set_body($value);
        if ($property_name === 'file')
            return $this->set_file($value);
        return false;
    }

    /**
     * @param string $method
     * @return bool
     */
    private function set_method($method)
    {
        $method = strtoupper($method);
        if ($method === 'GET')
            $this->method = 'GET';
        elseif ($method === 'HEAD')
            $this->method = 'HEAD';
        elseif ($method === 'POST')
            $this->method = 'POST';
        elseif ($method === 'PUT')
            $this->method = 'PUT';
        elseif ($method === 'DELETE')
            $this->method = 'DELETE';
        elseif ($method === 'OPTIONS')
            $this->method = 'OPTIONS';
        elseif ($method === 'TRACE')
            $this->method = 'TRACE';
        elseif ($method === 'CONNECT')
            $this->method = 'CONNECT';
        elseif ($method === 'PATCH')    // Appendix
            $this->method = 'PATCH';
        elseif ($method === 'LINK')     // Appendix
            $this->method = 'LINK';
        elseif ($method === 'UNLINK')   // Appendix
            $this->method = 'UNLINK';
        else
            return false;
        return true;
    }

    /**
     * @param string $url
     * @return bool
     */
    private function set_url($url)
    {
        $this->url = $url;
        $parsed = parse_url($url);
        if (!isset($parsed['scheme']))
            new \Exception('schemeが指定されていません');
        if (!isset($parsed['host']))
            new \Exception('hostが指定されていません');
        $url = $parsed['scheme'] . '://' . $parsed['host'];
        $path = '/';
        if (isset($parsed['path']) and $parsed['path'] !== '') {
            if (substr($parsed['path'], 0, 1) !== '/') {
                $path = '/' . $parsed['path'];
            } else {
                $path = $parsed['path'];
            }
        }
        $this->scheme = $parsed['scheme'];
        $this->host = $parsed['host'];
        $this->path = $path;
        if (isset($parsed['port']) and $parsed['port'] !== '') {
            $port = intval($parsed['port']);
            if (($parsed['scheme'] === 'http' and $port === 443)
                and ($parsed['scheme'] === 'https' and $port === 443)
                and ($parsed['scheme'] === 'ftp' and $port === 21)
                and ($parsed['scheme'] === 'gopher' and $port === 70)) {
                $url .= $path;
            } else {
                $url .= ':' . $port . $path;
            }
            $this->port = $port;
        } else {
            switch ($this->scheme) {
                case 'http':
                    $this->port = 80;
                    break;
                case 'https':
                    $this->port = 443;
                    break;
                case 'ftp':
                    $this->port = 21;
                    break;
                case 'gopher':
                    $this->port = 70;
                    break;
            }
            $url .= $path;
        }
        if (isset($parsed['query'])) {
            $url .= '?' . $parsed['query'];
            $this->query = $parsed['query'];
        } else {
            $this->query = null;
        }
        $this->url = $url;
        $this->fragment = isset($parsed['fragment']) ? $parsed['fragment'] : null;
        $this->user = isset($parsed['user']) ? $parsed['user'] : null;
        $this->password = isset($parsed['pass']) ? $parsed['pass'] : null;
        return true;
    }

    /**
     * @param string|null $body
     * @return bool
     */
    private function set_body($body)
    {
        $this->hasBody = true;
        $this->body = is_null($body) ? null : strval($body);
        return true;
    }

    /**
     * @param string|null $file
     * @return bool
     * @throws \Exception
     */
    private function set_file($file)
    {
        $this->hasBody = true;
        if (is_null($file)) {
            $this->file = null;
            return true;
        }
        if (!is_readable($file))
            throw new \Exception('cannot open file: ' . $file);
        $this->file = $file;
        return true;
    }

    /**
     * @param string $key
     * @param string $value
     * @return bool
     */
    public function addParameter($key, $value)
    {
        $this->hasBody = true;
        if (isset($this->parameters[$key])) {
            if (is_array($this->parameters[$key]))
                $this->parameters[$key][] = $value;
            else
                $this->parameters[$key] = [$this->parameters[$key], $value];
        } else {
            $this->parameters[$key] = $value;
        }
        return true;
    }

    /**
     * @param string $key
     * @param string $file
     * @return bool
     * @throws \Exception
     */
    public function addAttachment($key, $file)
    {
        $this->hasBody = true;
        if (!is_readable($file))
            throw new \Exception('cannot open file: ' . $file);
        if (isset($this->parameters[$key])) {
            if (is_array($this->parameters[$key]))
                $this->parameters[$key][] = new \CURLFile($file, mime_content_type($file));
            else
                $this->parameters[$key] = [$this->parameters[$key], new \CURLFile($file, mime_content_type($file))];
        } else {
            $this->parameters[$key] = new \CURLFile($file, mime_content_type($file));
        }
        $this->isAttached = true;
        return true;
    }

    /**
     * @return \Lumberyard\HTTPClient\Response
     * @throws \Exception
     */
    public function send()
    {
        if ($this->client)
            return $this->client->send($this);
        else
            throw new \Exception('no client');
    }
}