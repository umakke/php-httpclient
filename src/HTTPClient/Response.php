<?php
/**
 * Created by PhpStorm.
 * User: m.kiba
 * Date: 2018/10/18
 * Time: 13:08
 */

namespace Lumberyard\HTTPClient;

/**
 * Class Response
 * @package Lumberyard\HTTPClient
 * @property-read \Lumberyard\HTTPClient\Headers $headers
 * @property-read \Lumberyard\HTTPClient\Headers $header
 * @property-read int $status
 * @property-read int $size
 * @property-read string $content
 * @property-read string $error
 * @property-read \Lumberyard\HTTPClient\Response $before
 * @property-read string $URL
 * @property-read string $url
 * @property-read string $filename
 * @property-read float $response_time
 * @property-read float $time
 * @property-read string $bps
 * @property-read string $Mbps
 * @property-read string $Kbps
 * @method setError(string ...$errors);
 * @method addHeader(string $header_string)
 * @method addContent(string $content_string)
 * @method setRedirected(Response $response)
 * @method setResponseTime(float $response_time)
 */
class Response
{
    private $fixed = false;
    private $headers = null;
    private $http_version = 0;
    private $status = 0;
    private $status_string = '';
    private $error = null;
    private $size = 0;
    private $response_time = 0;
    private $_content = null;
    private $max_memory_size = 32 * 1024 * 1024;    // 32MB
    private $url = null;
    private $before = null;

    /**
     * Response constructor.
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
        $this->headers = new Headers();
    }

    /**
     * @param string $property_name
     * @return mixed
     * @throws \Exception
     */
    public function __get($property_name)
    {
        if ($property_name === 'content')
            return $this->content();
        if ($property_name === 'header' or $property_name === 'headers')
            return $this->headers;
        if ($property_name === 'status')
            return $this->status;
        if ($property_name === 'size')
            return $this->size;
        if ($property_name === 'before')
            return $this->before;
        if ($property_name === 'error')
            return $this->error;
        if ($property_name === 'url' or $property_name === 'URL')
            return $this->url;
        if ($property_name === 'response_time' or $property_name === 'time')
            return $this->response_time;
        if ($property_name === 'bps')
            return sprintf('%.3f', $this->size * 8 / $this->response_time);
        if ($property_name === 'Kbps')
            return sprintf('%.3f', ($this->size * 8 / $this->response_time) / 1024);
        if ($property_name === 'Mbps')
            return sprintf('%.3f', ($this->size * 8 / $this->response_time) / (1024 * 1024));
        if ($property_name === 'filename')
            return $this->_getFileName();
        throw new \Exception('unknown property: ' . $property_name);
    }

    /**
     *
     */
    public function __destruct()
    {
        // TODO: Implement __destruct() method.
        if ($this->_content)
            fclose($this->_content);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->content();
    }

    /**
     * @param string $method_name
     * @param array $arguments
     * @return mixed
     * @throws \Exception
     */
    public function __call($method_name, $arguments)
    {
        if (!$this->fixed) {
            if ($method_name === 'addHeader')
                return $this->_addHeader(...$arguments);
            if ($method_name === 'addContent')
                return $this->_addContent(...$arguments);
            if ($method_name === 'setError')
                return $this->_setError(...$arguments);
            if ($method_name === 'setRedirected')
                return $this->_setRedirected(...$arguments);
            if ($method_name === 'setResponseTime')
                return $this->_setResponseTime(...$arguments);
        }
        throw new \Exception('unknown method: ' . $method_name);
    }

    /**
     *
     */
    public function fix()
    {
        $this->fixed = true;
        $this->header->fix();
        if ($this->headers->get('Content-Length')) {
            $length = intval(strval($this->headers->get('Content-Length')));
            if ($length !== $this->size) {
                $this->_setError('file size error(header:', $length, '/real:', $this->size, ')');
            }
        }
    }

    /**
     * @param string $header_string
     * @return bool
     */
    private function _addHeader($header_string)
    {
        if (preg_match('/^HTTP\/([0-9]+\.[0-9]+)\s+([0-9]+)([^0-9]*)$/', $header_string, $m)) {
            $this->http_version = $m[1];
            $this->status = $m[2];
            $this->status_string = $m[3];
        } else {
            $parts = explode(':', $header_string, 2);
            if (count($parts) > 1) {
                $this->headers->add(trim($parts[0]), trim($parts[1]));
            }
        }
        return true;
    }

    /**
     * @param string $buffer
     * @return int
     */
    private function _addContent($buffer)
    {
        if (!$this->_content)
            $this->_content = fopen('php://temp/maxmemory:' . $this->max_memory_size, 'wb');
        $size = fwrite($this->_content, $buffer);
        $this->size += $size;
        return $size;
    }

    /**
     * @param string ...$errors
     * @return bool
     */
    private function _setError(...$errors)
    {
        $this->error = join('', $errors);
        return true;
    }

    /**
     * @param \Lumberyard\HTTPClient\Response $response
     * @return bool
     */
    private function _setRedirected($response)
    {
        $this->before = $response;
        return true;
    }

    /**
     * @param float $response_time
     * @return bool
     */
    private function _setResponseTime($response_time)
    {
        $this->response_time = $response_time;
        return true;
    }

    /**
     * @return string
     */
    private function _getFileName()
    {
        /**
         * @var \Lumberyard\HTTPClient\Header $disposition
         */
        $filename = null;
        $dispositions = $this->headers->get('Content-Disposition');
        if ($dispositions and $dispositions->length) {
            for ($i = 0; $i < $dispositions->length; $i++) {
                $disposition = $dispositions[$i];
                $parts = explode(';', $disposition);
                foreach ($parts as $part) {
                    $kv = explode('=', trim($part));
                    if ($kv[0] === 'filename*' and preg_match('/^utf-?8\'\'(.+)$/i', trim($kv[1], '" '), $m)) {
                        $filename = rawurldecode($m[1]);
                        break;
                    } elseif ($kv[0] === 'filename') {
                        $filename = trim($kv[1], '" ');
                        break;
                    }
                }
            }
            if ($filename)
                return $filename;
        }
        $path = parse_url($this->url, PHP_URL_PATH);
        if (!preg_match('#/$#', $path)) {
            $filename = basename($path);
            if ($filename !== '')
                return $filename;
        }
        return 'index.html';
    }

    /**
     * @param string $file_path
     * @param bool $overwrite
     * @return bool|string
     */
    public function save($file_path, $overwrite = false)
    {
        if (!$this->_content) {
            $this->_setError('no content');
            return false;
        }
        if (!is_writable(dirname($file_path))) {
            $this->_setError('Can\'t create file:', $file_path);
            return false;
        }
        if (file_exists($file_path)) {
            if ($overwrite === false) {
                if (preg_match('/\.[a-z0-9]+$/', $file_path)) {
                    $file_path_ex = preg_replace('/\.([a-z0-9]+)$/', '(1).\1', $file_path);
                    $i = 1;
                    while (file_exists($file_path_ex)) {
                        $i++;
                        $file_path_ex = preg_replace('/\.([a-z0-9]+)$/', '(' . $i . ').\1', $file_path);
                    }
                    $file_path = $file_path_ex;
                } else {
                    $file_path_ex = $file_path . '(1)';
                    $i = 1;
                    while (file_exists($file_path_ex)) {
                        $i++;
                        $file_path_ex = $file_path . '(' . $i . ')';
                    }
                    $file_path = $file_path_ex;
                }
            }
        }
        $fp = @fopen($file_path, 'w');
        if (!$fp) {
            $this->_setError('Can\'t open file:', $file_path);
            return false;
        }
        flock($fp, LOCK_EX);
        fseek($this->_content, 0, SEEK_SET);
        while (!feof($this->_content)) {
            fwrite($fp, fread($this->_content, 4096));
        }
        fclose($fp);
        return $file_path;
    }

    /**
     * @return string
     */
    public function content()
    {
        $buffer = '';
        if ($this->_content) {
            fseek($this->_content, 0, SEEK_SET);
            while (!feof($this->_content)) {
                $buffer .= fread($this->_content, 4096);
            }
        }
        return $buffer;
    }

    public function __debugInfo()
    {
        return [
            'url' => $this->url,
            'filename' => $this->filename,
            'status' => $this->status,
            'headers' => $this->headers,
            'error' => $this->error,
            'size' => $this->size,
            'time' => $this->time,
            'bps' => $this->bps,
            'Kbps' => $this->Kbps,
            'Mbps' => $this->Mbps,
        ];
    }
}