<?php
/**
 * Use to autoload needed classes without Composer.
 *
 * @param string $class The fully-qualified class name.
 * @return void
 */
spl_autoload_register(function ($class) {
    $base_name = 'Lumberyard\\HTTPClient';
    $length = strlen($base_name);
    if (substr($class, 0, strlen($base_name)) !== 'Lumberyard\\HTTPClient')
        return;
    $relative_class = 'HTTPClient' . substr($class, $length);
    $base_dir = __DIR__ . '/src/';
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
    if (file_exists($file)) {
        require $file;
    }
});
